package com.tech.bootstrap.models;

public class BootStrapColumnRequestModel {
	public String data;
	public String name;
	public boolean searchable;
	public boolean orderable;
	
	public BootStrapColumnRequestModel(){
		
	}
}
