package com.tech.bootstrap.models;

public class BootStrapUtils {

	public static String insertEvery(String input, char sepator, int index) {
		StringBuilder output = new StringBuilder();
		int pointer = 0;
		for(int i=0;i<input.length();i++){
			if(i==0){
				
			}else if((pointer)%index==0){
				output.append(sepator);
				pointer = 0;
			}
			output.append(input.charAt(i));
			pointer++;
		}
		return output.toString();
	}

}
