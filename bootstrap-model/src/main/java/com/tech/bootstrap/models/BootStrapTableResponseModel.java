package com.tech.bootstrap.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BootStrapTableResponseModel<T> {
	public List<Map<String, Object>> data;
	public int draw;
	public int recordTotal;
	public int recordFiltered;
	public int iTotalDisplayRecords;
	public int iTotalRecords;
	
	public void initial(int totalData, int totalFiltered,
			BootStrapTableRequestModel model, List<T> datas,
			PutDataOper<T> putDataOper) {

		List<Map<String, Object>> shouldDisplay = new ArrayList<>();
		for (T gr : datas) {
			Map<String, Object> data = new HashMap<>();
			putDataOper.put(data, gr);
			shouldDisplay.add(data);
		}

		List<Map<String, Object>> res = new ArrayList<>();
		this.data = res;
		res.addAll(shouldDisplay);

		draw = model.draw;
		recordFiltered = res.size();
		recordTotal = res.size();
		/**
		 * iTotalDisplayRecords is NOT the number of records being displayed on
		 * the page, it is the number of records that would be displayed, after
		 * filtering, if there was no pagination.
		 */
		if(model.search.keySet().isEmpty()){
			iTotalDisplayRecords = totalData;
		}else{
			iTotalDisplayRecords = totalFiltered;
		}
		
		iTotalRecords = totalData;

	}

	public void initial(BootStrapTableRequestModel model, List<T> datas,
			FilterOper<T> filterOper, PutDataOper<T> putDataOper) {
		List<T> filtereds = new ArrayList<>();
		if (model.search.keySet().isEmpty()) {
			filtereds.addAll(datas);
		} else {
			for (T gr : datas) {
				if (filterOper.filter(gr, model)) {
					filtereds.add(gr);
				}
			}
		}

		List<Map<String, Object>> shouldDisplay = new ArrayList<>();
		for (T gr : filtereds) {
			Map<String, Object> data = new HashMap<>();
			putDataOper.put(data, gr);
			shouldDisplay.add(data);
		}
		Collections.sort(shouldDisplay, new Comparator<Map<String, Object>>() {
			@Override
			public int compare(Map<String, Object> o1, Map<String, Object> o2) {
				for (BootStrapOrderRequestModel or : model.order) {
					int ret;
					if (o1.get(model.columns[or.column].data) == null
							|| o2.get(model.columns[or.column].data) == null) {
						continue;
					}
					if (or.dir.equals("asc")) {
						ret = o1.get(model.columns[or.column].data)
								.toString()
								.compareTo(
										o2.get(model.columns[or.column].data)
												.toString());
					} else {
						ret = o2.get(model.columns[or.column].data)
								.toString()
								.compareTo(
										o1.get(model.columns[or.column].data)
												.toString());
					}
					if (ret == 0) {
						continue;
					}
					return ret;
				}
				return 0;
			}
		});

		List<Map<String, Object>> res = new ArrayList<>();
		this.data = res;
		int start = -1;
		for (Map<String, Object> gr : shouldDisplay) {
			start++;
			if (start < model.start) {
				continue;
			}
			res.add(gr);
			if (res.size() == model.length) {
				break;
			}
		}

		draw = model.draw;
		recordFiltered = res.size();
		recordTotal = datas.size();
		/**
		 * iTotalDisplayRecords is NOT the number of records being displayed on
		 * the page, it is the number of records that would be displayed, after
		 * filtering, if there was no pagination.
		 */
		iTotalDisplayRecords = filtereds.size();
		iTotalRecords = datas.size();

	}
}
