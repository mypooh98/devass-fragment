package com.tech.bootstrap.models;

import static org.junit.Assert.*;

import org.junit.Test;

public class BootStrapUtilsTest {

	@Test
	public void test() {
		assertTrue(true);
	}
	
	@Test
	public void testInsertCharToString(){
		String input = "Owner_Checker";
		String expect = "Owner _Chec ker";
		String output = BootStrapUtils.insertEvery(input, ' ', 5);
		System.out.println(expect);
		System.out.println(output);
		assertEquals(expect, output);
	}

}
