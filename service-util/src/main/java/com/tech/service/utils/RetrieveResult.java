package com.tech.service.utils;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

public class RetrieveResult<T> {

	private int totalData;
	private int totalFiltered;
	public List<T> datas;
	private RetrieveCriteria retreiveCriteria;

	public RetrieveResult(int totalData, List<T> datas,
			RetrieveCriteria retreiveCriteria) {
		super();
		this.totalData = totalData;
		this.datas = datas;
		this.retreiveCriteria = retreiveCriteria;
	}

	@SuppressWarnings("unchecked")
	public RetrieveResult(JdbcTemplate jdbcTemplate, String sql,
			Object[] params, RetrieveCriteria retrieveCriteria,
			Class<T> requireType) {
		this.retreiveCriteria = retrieveCriteria;

		StringBuilder countTotalSql = new StringBuilder();
		countTotalSql.append(" SELECT COUNT(*) FROM ( ");
		countTotalSql.append(sql.toString()).append(" ) ");
		totalData = jdbcTemplate.queryForObject(countTotalSql.toString(),
				params, Integer.class);
		
		StringBuilder countTotalFilteredSql = new StringBuilder();
		countTotalFilteredSql.append(" SELECT COUNT(*) FROM ( ");
		countTotalFilteredSql.append("         SELECT * FROM( ");
		countTotalFilteredSql.append("             SELECT * FROM( ");
		countTotalFilteredSql.append(" ").append(sql).append(" ");
		countTotalFilteredSql.append("             ) ");
		countTotalFilteredSql.append("             ");
		if(retrieveCriteria.haveFilters()){
			countTotalFilteredSql.append(" WHERE ").append(retrieveCriteria.getFiltersStr());
		}
		countTotalFilteredSql.append(" 			) ");
		countTotalFilteredSql.append(" ) ");
		totalFiltered = jdbcTemplate.queryForObject(countTotalFilteredSql.toString(),
				params, Integer.class);

		StringBuilder paggingSql = new StringBuilder();
		paggingSql.append(" SELECT * FROM ");
		paggingSql.append(" ( ");
		paggingSql.append("     SELECT a.*, rownum r__ ");
		paggingSql.append("     FROM ");
		paggingSql.append("     ( ");
		paggingSql.append("         SELECT * FROM( ");
		paggingSql.append("             SELECT * FROM( ");
		paggingSql.append(" ").append(sql).append(" ");
		paggingSql.append("             ) ");
		paggingSql.append("             ");
		if(retrieveCriteria.haveFilters()){
			paggingSql.append(" WHERE ").append(retrieveCriteria.getFiltersStr());
		}
		paggingSql.append("         )");
		if(retrieveCriteria.haveOrders()){
			paggingSql.append(" ORDER BY ").append(retrieveCriteria.getOrdersStr());
		}
		paggingSql.append("     ) a ");
		paggingSql.append("     WHERE rownum < ((? * ?) + 1 ) ");
		paggingSql.append(" ) ");
		paggingSql.append(" WHERE r__ >= (((?-1) * ?) + 1) ");
		
		Object[]allParam = new Object[params.length + 4];
		int index = 0;
		for(int i=0;i<params.length;i++){
			allParam[index] = params[i];
			index++;
		}
		allParam[index++] = retrieveCriteria.getCurrentPage();
		allParam[index++] = retrieveCriteria.getPageSize();
		allParam[index++] = retrieveCriteria.getCurrentPage();
		allParam[index++] = retrieveCriteria.getPageSize();
		
		if (requireType == Map.class) {
			datas = (List<T>) jdbcTemplate.queryForList(
					paggingSql.toString(), allParam
					);
		} else {
			datas = jdbcTemplate.queryForList(
					paggingSql.toString(),
					new Object[] { retrieveCriteria.getCurrentPage(),
							retrieveCriteria.getPageSize(),
							retrieveCriteria.getCurrentPage(),
							retrieveCriteria.getPageSize() }, requireType);
		}

	}

	public int getTotalFiltered() {
		return totalFiltered;
	}

	public int getTotalData() {
		return totalData;
	}

	public RetrieveCriteria getRetrieveCriteria() {
		return retreiveCriteria;
	}

	public List<T> getDatas() {
		return datas;
	}

}
