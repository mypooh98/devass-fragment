package com.tech.service.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RetrieveCriteria {

	private int pageSize;
	private int currentPage;
	public List<String> filters;
	public List<String> orders;
	public List<Map<String,Object>> params;

	public List<Map<String, Object>> getParams() {
		return params;
	}

	public void setParams(List<Map<String, Object>> params) {
		this.params = params;
	}

	public void setFilters(List<String> filters) {
		this.filters = new ArrayList<>();
		for(String fl:filters){
			this.filters.add(fl.replace(";", ""));
		}
	}

	public void setOrders(List<String> orders) {
		this.orders = new ArrayList<>();
		for(String or:orders){
			this.orders.add(or.replace(";", ""));
		}
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public List<String> getFilters() {
		return filters;
	}

	public List<String> getOrders() {
		return orders;
	}

	public String getFiltersStr() {
		if(filters == null){
			return "";
		}
		StringBuilder res = new StringBuilder();
		for(int i=0;i<filters.size();i++){
			if(i>0){
				res.append(" AND ");
			}
			res.append(filters.get(i));
		}
		return res.toString();
	}
	
	public String getOrdersStr() {
		if(orders == null){
			return "";
		}
		StringBuilder res = new StringBuilder();
		for(int i=0;i<orders.size();i++){
			if(i>0){
				res.append(" , ");
			}
			res.append(orders.get(i));
		}
		return res.toString();
	}
	
	public boolean haveFilters(){
		return filters != null && !filters.isEmpty();
	}
	
	public boolean haveOrders(){
		return orders != null && !orders.isEmpty();
	}
}
