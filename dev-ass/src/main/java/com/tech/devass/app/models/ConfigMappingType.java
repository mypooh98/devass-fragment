package com.tech.devass.app.models;

public class ConfigMappingType {
	private String databaseType, javaType, getType;

	public ConfigMappingType(String databaseType, String javaType,
			String getType) {
		this.databaseType = databaseType;
		this.javaType = javaType;
		this.getType = getType;
	}

	public String getDatabaseType() {
		return databaseType;
	}

	public String getJavaType() {
		return javaType;
	}

	public String getGetType() {
		return getType;
	}

}
