package com.tech.devass.app.models;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ConfigModel {
	private List<ConfigMappingType> mappingTypes;
	private List<ConfigSkipColumn> skipCols;
	private List<ConfigSkipType> skipTypes;

	private ConfigModel(List<ConfigMappingType> mappingTypes,
			List<ConfigSkipColumn> skipCols, List<ConfigSkipType> skipTypes) {
		this.mappingTypes = mappingTypes;
		this.skipCols = skipCols;
		this.skipTypes = skipTypes;
	}

	public List<ConfigMappingType> getMappingTypes() {
		return mappingTypes;
	}

	public List<ConfigSkipColumn> getSkipCols() {
		return skipCols;
	}

	public List<ConfigSkipType> getSkipTypes() {
		return skipTypes;
	}

	public static ConfigModel parse(String configFilePath)
			throws FileNotFoundException, IOException {

		XSSFWorkbook configFile = new XSSFWorkbook(new FileInputStream(
				configFilePath));

		XSSFSheet mtSheet = configFile.getSheet("mapping types");
		XSSFSheet scSheet = configFile.getSheet("skip columns");
		XSSFSheet stSheet = configFile.getSheet("skip types");

		List<ConfigMappingType> mappingTypes = new ArrayList<>();
		int rowIndex = 1;
		String databaseType, javaType, getType;
		do {
			databaseType = read(mtSheet, rowIndex, 0);
			javaType = read(mtSheet, rowIndex, 1);
			getType = read(mtSheet, rowIndex, 2);
			rowIndex++;
			ConfigMappingType cf = new ConfigMappingType(databaseType,
					javaType, getType);
			if (databaseType != null) {
				mappingTypes.add(cf);
			}
		} while (databaseType != null);

		List<ConfigSkipColumn> skipCols = new ArrayList<>();
		rowIndex = 1;
		String oper, partOfColumnName;
		do {
			oper = read(scSheet, rowIndex, 0);
			partOfColumnName = read(scSheet, rowIndex, 1);
			rowIndex++;
			ConfigSkipColumn cf = new ConfigSkipColumn(oper, partOfColumnName);
			if (partOfColumnName != null) {
				skipCols.add(cf);
			}
		} while (partOfColumnName != null);
		
		List<ConfigSkipType> skipTypes = new ArrayList<>();
		rowIndex = 1;
		String partOfTypeName;
		do {
			oper = read(stSheet, rowIndex, 0);
			partOfTypeName = read(stSheet, rowIndex, 1);
			rowIndex++;
			ConfigSkipType cf = new ConfigSkipType(oper, partOfTypeName);
			if (partOfTypeName != null) {
				skipTypes.add(cf);
			}
		} while (partOfTypeName != null);

		return new ConfigModel(mappingTypes, skipCols, skipTypes);
	}

	private static String read(XSSFSheet mySheet, int rowNum, int colNum) {
		XSSFRow row = mySheet.getRow(rowNum);
		if(row == null){
			return null;
		}
		XSSFCell cell = row.getCell(colNum);
		return cell.getStringCellValue();
	}

}
