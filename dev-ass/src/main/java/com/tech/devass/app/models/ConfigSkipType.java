package com.tech.devass.app.models;

public class ConfigSkipType {
	private String oper, partOfTypeName;

	public ConfigSkipType(String oper, String partOfTypeName) {
		this.oper = oper;
		this.partOfTypeName = partOfTypeName;
	}

	public String getOper() {
		return oper;
	}

	public String getPartOfTypeName() {
		return partOfTypeName;
	}

}
