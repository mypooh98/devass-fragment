package com.tech.devass.app.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Controller
@EnableWebMvc
public class DevAssApplication extends WebMvcConfigurerAdapter {

	private static final Logger logger = LogManager
			.getLogger(DevAssApplication.class);

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/js/main.js").setViewName("main.js");
		// registry.addViewController("/oauth/confirm_access").setViewName(
		// "authorize");
		registry.addViewController("/").setViewName("index");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/js/**").addResourceLocations("/js/")
				.setCachePeriod(31556926);
		registry.addResourceHandler("/css/**").addResourceLocations("/css/")
				.setCachePeriod(31556926);
		registry.addResourceHandler("/views/**")
				.addResourceLocations("/views/").setCachePeriod(31556926);
		registry.addResourceHandler("/tpl/**").addResourceLocations("/tpl/")
				.setCachePeriod(31556926);
		registry.addResourceHandler("/static/**")
				.addResourceLocations("/WEB-INF/static/")
				.setCachePeriod(31556926);

		// for jboss because it No webjar with uri:
		// webjar:jquery/2.1.1/jquery.min.js available. happen
		registry.addResourceHandler("/wro/**").addResourceLocations("/wro/")
				.setCachePeriod(31556926);
		registry.addResourceHandler("/images/**")
				.addResourceLocations("/images/").setCachePeriod(31556926);
		registry.addResourceHandler("/fonts/**")
				.addResourceLocations("/fonts/").setCachePeriod(31556926);
	}

	@Configuration
	@ComponentScan(basePackages =

	"com.tech.authorize.app.daos,com.tech.devass.app.services,com.tech.devass.app.controllers,"
	+ "com.tech.devass.app.bootstrap.controllers,com.tech.devass.app.bootstrap.models")
	@PropertySource("classpath:/application.properties")
	protected static class DevAssConfig {
		
		@Autowired
        private Environment env;
		
		@Bean(name="devAssEnv")
		public Environment env(){
			return env;
		}
		
		@Bean
		public org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer freeMarkerConfigurer() {
			org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer ret = new org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer();
			ret.setTemplateLoaderPath("/WEB-INF/templates/");
			return ret;
		}

		@Bean
		public org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver freeMarkerViewResolver() {
			org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver ret = new org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver();
			ret.setCache(true);
			ret.setPrefix("");
			ret.setSuffix(".ftl");
			return ret;
		}

		// <!-- บน wildfly ถ้าไม่ใส่ ก็รันได้ แต่บน tomcat ไม่ใส่จะรันไม่ได้ -->
		@Bean
		public HttpMessageConverter<?> customConverters() {
			HttpMessageConverter<?> jacksonMessageConverter = new MappingJackson2HttpMessageConverter();
			return jacksonMessageConverter;
		}

		@Bean
        public org.apache.commons.dbcp.BasicDataSource dataSourceOracle() {
            org.apache.commons.dbcp.BasicDataSource ds
                    = new org.apache.commons.dbcp.BasicDataSource();
            ds.setDriverClassName(env.getProperty("ds.driverClassName"));
            ds.setUrl(env.getProperty("ds.url"));
            ds.setUsername(env.getProperty("ds.username"));
            ds.setPassword(env.getProperty("ds.password"));
            ds.setInitialSize(env.getProperty("ds.initialSize", Integer.TYPE));
            ds.setMaxActive(env.getProperty("ds.maxActive", Integer.TYPE));
            ds.setDefaultAutoCommit(env.getProperty("ds.defaultAutoCommit", Boolean.TYPE));
            return ds;
        }
		
		@Bean
        public org.springframework.jdbc.core.JdbcTemplate jdbcTemplate(
                org.apache.commons.dbcp.BasicDataSource ds) {
            org.springframework.jdbc.core.JdbcTemplate jd
                    = new org.springframework.jdbc.core.JdbcTemplate();
            jd.setDataSource(ds);
            return jd;
        }
	}

}
