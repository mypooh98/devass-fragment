package com.tech.devass.app.models;

public class JavaTypeDef {

	private String javaType;
	private String resultSetGet;

	public JavaTypeDef(String javaType, String resultSetGet) {
		this.javaType = javaType;
		this.resultSetGet = resultSetGet;
	}

	public String getJavaType() {
		return javaType;
	}

	public String getResultSetGet() {
		return resultSetGet;
	}
}
