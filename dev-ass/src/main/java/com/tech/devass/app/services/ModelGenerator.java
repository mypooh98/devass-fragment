package com.tech.devass.app.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.tech.devass.app.models.ConfigMappingType;
import com.tech.devass.app.models.ConfigModel;
import com.tech.devass.app.models.ConfigSkipColumn;
import com.tech.devass.app.models.ConfigSkipType;
import com.tech.devass.app.models.DBTabInfo;
import com.tech.devass.app.models.GenerateResult;
import com.tech.devass.app.models.JavaTypeDef;

class DBColumn {

	String name, type, constraint;
}

@Service("modelGenerator")
public class ModelGenerator {

	String[] skipColumnName = { "SYS_STS", "SYS_NC" };
	String[] skipColumnType = { "BLOB" };

	@Autowired
	JdbcTemplate jd;

	@Autowired
	@Qualifier("devAssEnv")
	Environment env;

	public ModelGenerator() {
	}

	public int countTables() throws Exception {
		String listTableCmd = "SELECT table_name FROM user_tables";
		List<Map<String, Object>> tables = jd.queryForList(listTableCmd);
		return tables.size();
	}

	public List<GenerateResult> summaryTables() throws Exception {

		ConfigModel configModel = ConfigModel.parse(env
				.getProperty("config.resource.path"));

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT TC.TABLE_NAME,TC.COLUMN_NAME,TC.DATA_TYPE,  ");
		sql.append(" LISTAGG(CONS.CONSTRAINT_TYPE, ',') WITHIN GROUP (ORDER BY CONS.CONSTRAINT_TYPE DESC) AS CONSKEY ");
		sql.append(" FROM USER_TAB_COLS TC ");
		sql.append(" LEFT JOIN ALL_CONS_COLUMNS COLS ON COLS.COLUMN_NAME=TC.COLUMN_NAME AND COLS.OWNER = ? ");
		sql.append(" LEFT JOIN ALL_CONSTRAINTS CONS ON CONS.CONSTRAINT_NAME=COLS.CONSTRAINT_NAME  ");
		sql.append("  AND CONS.OWNER = ? AND CONS.TABLE_NAME = TC.TABLE_NAME ");
		sql.append(" GROUP BY TC.TABLE_NAME,TC.COLUMN_NAME,TC.DATA_TYPE ");
		sql.append(" ORDER BY TC.TABLE_NAME ");


		List<Map<String, Object>> tablesInfo = jd.queryForList(
				sql.toString(),
				new Object[] { env.getProperty("ds.username"),
						env.getProperty("ds.username") });

		List<DBTabInfo> tables = new ArrayList<>();
		for (Map<String, Object> tb : tablesInfo) {
			DBTabInfo sr = new DBTabInfo(tb.get("TABLE_NAME").toString());
			if (!tables.contains(sr)) {
				tables.add(sr);
			} else {
				sr = tables.get(tables.indexOf(sr));
			}
			sr.addCol(
					tb.get("COLUMN_NAME").toString(),
					tb.get("DATA_TYPE") == null ? null : tb.get("DATA_TYPE")
							.toString(),
					tb.get("CONSKEY") == null ? null : tb.get(
							"CONSKEY").toString());
		}

		Map<String, JavaTypeDef> typeDefinite = new HashMap<>();
		// typeDefinite.put("NUMBER", new JavaTypeDef("int", "getInt"));
		// typeDefinite.put("NVARCHAR2", new JavaTypeDef("String",
		// "getString"));
		// typeDefinite.put("VARCHAR2", new JavaTypeDef("String", "getString"));
		// typeDefinite.put("TIMESTAMP(6) WITH TIME ZONE",
		// new JavaTypeDef("Date", "getTimestamp"));
		// typeDefinite.put("DATE", new JavaTypeDef("Date", "getTimestamp"));
		for (ConfigMappingType mt : configModel.getMappingTypes()) {
			typeDefinite.put(mt.getDatabaseType(),
					new JavaTypeDef(mt.getJavaType(), mt.getGetType()));
		}

		DAOGenerator daoGenerator = new DAOGenerator();
		daoGenerator.setTypeDefinite(typeDefinite);

		EntityGenerator entityGenerator = new EntityGenerator();
		entityGenerator.setTypeDefinite(typeDefinite);

		for (ConfigSkipColumn column : configModel.getSkipCols()) {
			daoGenerator.addSkipColumns(column);
			entityGenerator.addSkipColumns(column);
		}
		for (ConfigSkipType type : configModel.getSkipTypes()) {
			daoGenerator.addSkipType(type);
			entityGenerator.addSkipType(type);
		}

		List<GenerateResult> result = new ArrayList<>();
		for (DBTabInfo tb : tables) {
			GenerateResult gr = new GenerateResult();
			gr.name = tb.tableName;
			gr.keys = daoGenerator.getKeys(tb.columns).toString();
			String status;
			if (daoGenerator.getKeys(tb.columns).isEmpty()) {
				status = "NOPK";
				gr.daoCode = "";
				gr.entityCode = "";
			} else {
				status = "OK";
				gr.daoCode = daoGenerator.gen(tb);
				gr.entityCode = entityGenerator.gen(tb);
			}
			gr.status = status;
			result.add(gr);
		}

		return result;
	}
}
