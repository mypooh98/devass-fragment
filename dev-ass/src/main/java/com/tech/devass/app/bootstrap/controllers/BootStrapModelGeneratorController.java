package com.tech.devass.app.bootstrap.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tech.bootstrap.models.BootStrapTableRequestModel;
import com.tech.bootstrap.models.BootStrapTableResponseModel;
import com.tech.bootstrap.models.FilterOper;
import com.tech.bootstrap.models.PutDataOper;
import com.tech.devass.app.models.GenerateResult;
import com.tech.devass.app.services.ModelGenerator;

@Controller
@RequestMapping("/bootstrap/modelgen")
public class BootStrapModelGeneratorController {

	@Autowired
	ModelGenerator modelGenerator;

	@RequestMapping(value = "/summaryTables", method = RequestMethod.POST)
	public @ResponseBody BootStrapTableResponseModel<GenerateResult> summaryTables(
			HttpServletRequest request) throws Exception {
		Map<String, String[]> reqModel = request.getParameterMap();
		BootStrapTableRequestModel model = BootStrapTableRequestModel
				.parse(reqModel);

		List<GenerateResult> tables = modelGenerator.summaryTables();

		BootStrapTableResponseModel<GenerateResult> responseObj = new BootStrapTableResponseModel<>();
		responseObj.initial(model, tables, new FilterOper<GenerateResult>() {
			@Override
			public boolean filter(GenerateResult gr,
					BootStrapTableRequestModel model) {
				return gr.name.contains(model.search.get("name"));
			}
		}, new PutDataOper<GenerateResult>() {
			@Override
			public void put(Map<String, Object> data, GenerateResult gr) {
				data.put("checkbox",
						"<input type=\"checkbox\" name=\"id[]\" value=\"21\">");
				data.put("name", gr.name);
				data.put("keys", gr.keys);
				String status;
				if (gr.status.equals("OK")) {
					status = "<span class=\"label label-sm label-success\"> OK </span>";
				} else {
					status = "<span class=\"label label-sm label-warning\"> NO PK </span>";
				}
				data.put("status", status);
				data.put("daoCode", gr.daoCode);
				data.put("entityCode", gr.entityCode);
			}
		});

		return responseObj;
	}
}
