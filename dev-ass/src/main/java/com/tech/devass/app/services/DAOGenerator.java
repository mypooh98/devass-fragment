package com.tech.devass.app.services;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.CaseFormat;
import com.tech.devass.app.models.DBColInfo;
import com.tech.devass.app.models.DBTabInfo;
import com.tech.devass.app.utils.StringUtils;

public class DAOGenerator extends EntityGenerator {

	@Override
	public String gen(DBTabInfo dBTabInfo) {

		List<DBColInfo> cols = new ArrayList<>();
		for (DBColInfo col : dBTabInfo.columns) {
			if (checkSkipCondition(col)) {
				continue;
			}
			cols.add(col);
		}

		List<String> keys;
		if ((keys = getKeys(cols)).size() == 0) {
			throw new IllegalArgumentException("AT LEAST ONE PRIMARY KEY");
		}

		String className = CaseFormat.UPPER_UNDERSCORE.to(
				CaseFormat.UPPER_CAMEL, dBTabInfo.tableName);

		StringBuilder output = new StringBuilder();
		StringUtils.appendLine(output, "    @Autowired ");
		StringUtils.appendLine(output,
				"    private JdbcTemplate jdbcTemplate; ");
		StringUtils
				.appendLine(output,
						"    protected final static String SQL_SELECT = buildSelectStatement(); ");
		StringUtils
				.appendLine(
						output,
						"    protected final static String SQL_SELECT_ONE = buildSelectOneStatement(); ");
		StringUtils
				.appendLine(output,
						"    protected final static String SQL_INSERT = buildInsertStatement(); ");
		StringUtils
				.appendLine(output,
						"    protected final static String SQL_UPDATE = buildUpdateStatement(); ");
		StringUtils
				.appendLine(output,
						"    protected final static String SQL_DELETE = buildDeleteStatement(); ");

		/**
		 * Begin RowMapper
		 */
		StringUtils.appendLine(output, "    private final static RowMapper<"
				+ className + "> rowMapper" + className + " = new RowMapper<"
				+ className + ">() { ");
		StringUtils.appendLine(output, "		@Override ");
		StringUtils.appendLine(output, "		public " + className
				+ " mapRow(ResultSet rs, int rowNum) throws SQLException { ");
		StringUtils.appendLine(output, "			" + className + " dto = new "
				+ className + "(); ");

		for (DBColInfo colInfo : cols) {

			String attributeName = CaseFormat.UPPER_UNDERSCORE.to(
					CaseFormat.UPPER_CAMEL, colInfo.name);
			String setMethod = "			dto.set" + attributeName;
			String parameter = "(rs." + toResultSetGet(colInfo.type) + "(\""
					+ colInfo.name + "\")" + ");";

			StringUtils.appendLine(output, setMethod + parameter);
			
			String checkNull = "			if(rs.wasNull()){\n";
			checkNull += "	" + setMethod+"(null);\n";
			checkNull += "			}\n";
			StringUtils.appendLine(output, checkNull);
		}
		StringUtils.appendLine(output, "			return dto; ");
		StringUtils.appendLine(output, "		} ");
		StringUtils.appendLine(output, "    }; ");
		/**
		 * End RowMapper
		 */

		/**
		 * Begin BuildCommand
		 */
		StringUtils.appendLine(output,
				"    private static String buildSelectStatement() { ");
		StringUtils.appendLine(output,
				"        StringBuilder sql = new StringBuilder(); ");
		StringUtils.appendLine(output, "        sql.append(\" SELECT \"); ");
		for (int i = 0; i < cols.size(); i++) {
			DBColInfo col = cols.get(i);
			if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ col.name + " \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   ,"
						+ col.name + " \"); ");
			}
		}
		StringUtils.appendLine(output, "        sql.append(\" FROM "
				+ dBTabInfo.tableName + " \"); ");
		StringUtils.appendLine(output, "        return sql.toString(); ");
		StringUtils.appendLine(output, "    } ");

		StringUtils.appendLine(output,
				"    private static String buildSelectOneStatement() { ");
		StringUtils.appendLine(output,
				"        StringBuilder sql = new StringBuilder(); ");
		StringUtils.appendLine(output, "        sql.append(\" SELECT \"); ");
		for (int i = 0; i < cols.size(); i++) {
			DBColInfo col = cols.get(i);
			if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ col.name + " \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   ,"
						+ col.name + " \"); ");
			}
		}
		StringUtils.appendLine(output, "        sql.append(\" FROM "
				+ dBTabInfo.tableName + "\"); ");
		StringUtils.appendLine(output, "        sql.append(\" WHERE \"); ");
		for (int i = 0; i < keys.size(); i++) {
			if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ keys.get(i) + " = ? \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   AND "
						+ keys.get(i) + " = ? \"); ");
			}
		}
		StringUtils.appendLine(output, "        return sql.toString(); ");
		StringUtils.appendLine(output, "    } ");

		StringUtils.appendLine(output,
				"    private static String buildInsertStatement() { ");
		StringUtils.appendLine(output,
				"        StringBuilder sql = new StringBuilder(); ");
		StringUtils.appendLine(output, "        sql.append(\" INSERT INTO "
				+ dBTabInfo.tableName + " ( \"); ");
		StringBuilder values = new StringBuilder();
		for (int i = 0; i < cols.size(); i++) {
			DBColInfo col = cols.get(i);
			if (i > 0 && i == cols.size() - 1) {
				StringUtils.appendLine(output, "        sql.append(\"   ,"
						+ col.name + ") \"); ");
			} else if (i == 0 && i == cols.size() - 1) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ col.name + ") \"); ");
			} else if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ col.name + " \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   ,"
						+ col.name + " \"); ");
			}
			if (i > 0) {
				values.append(",");
			}
			values.append("?");
		}
		StringUtils.appendLine(output, "        sql.append(\" VALUES ("
				+ values.toString() + ") \"); ");
		StringUtils.appendLine(output, "        return sql.toString(); ");
		StringUtils.appendLine(output, "    } ");

		StringUtils.appendLine(output,
				"    private static String buildUpdateStatement() { ");
		StringUtils.appendLine(output,
				"        StringBuilder sql = new StringBuilder(); ");
		StringUtils.appendLine(output, "        sql.append(\" UPDATE "
				+ dBTabInfo.tableName + " \"); ");
		StringUtils.appendLine(output, "        sql.append(\" SET \"); ");
		for (int i = 0; i < cols.size(); i++) {
			DBColInfo col = cols.get(i);
			if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ col.name + " = ? \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   ,"
						+ col.name + " = ? \"); ");
			}
		}
		StringUtils.appendLine(output, "        sql.append(\" WHERE \"); ");
		for (int i = 0; i < keys.size(); i++) {
			if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ keys.get(i) + " = ? \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   AND "
						+ keys.get(i) + " = ? \"); ");
			}
		}
		StringUtils.appendLine(output, "        return sql.toString(); ");
		StringUtils.appendLine(output, "    } ");

		StringUtils.appendLine(output,
				"    private static String buildDeleteStatement() { ");
		StringUtils.appendLine(output,
				"        StringBuilder sql = new StringBuilder(); ");
		StringUtils.appendLine(output, "        sql.append(\" DELETE FROM "
				+ dBTabInfo.tableName + " \"); ");
		StringUtils.appendLine(output, "        sql.append(\" WHERE \"); ");
		for (int i = 0; i < keys.size(); i++) {
			if (i == 0) {
				StringUtils.appendLine(output, "        sql.append(\"   "
						+ keys.get(i) + " = ? \"); ");
			} else {
				StringUtils.appendLine(output, "        sql.append(\"   AND "
						+ keys.get(i) + " = ? \"); ");
			}
		}
		StringUtils.appendLine(output, "        return sql.toString(); ");
		StringUtils.appendLine(output, "    } ");
		/**
		 * End BuildCommand
		 */

		/**
		 * Begin SIUD
		 */
		StringUtils.appendLine(output, "    public List<" + className
				+ "> selectAll() {");
		StringUtils.appendLine(output, "        List<" + className
				+ "> resultList = jdbcTemplate.query(SQL_SELECT, rowMapper"
				+ className + ");");
		StringUtils.appendLine(output, "        return resultList;");
		StringUtils.appendLine(output, "    }");

		StringUtils.appendLine(output, "    public " + className
				+ " selectOne(" + className + " dto) {");
		StringUtils.appendLine(output, "        List<" + className
				+ "> resultList = jdbcTemplate.query(SQL_SELECT_ONE, rowMapper"
				+ className + ", ");
		for (int i = 0; i < keys.size(); i++) {
			if (i > 0 && i == keys.size() - 1) {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "() });");
			} else if (i == 0 && keys.size() > 1) {
				StringUtils.appendLine(
						output,
						"						new Object[] { dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "(), ");
			} else if (i == 0 && keys.size() == 1) {
				StringUtils.appendLine(
						output,
						"						new Object[] { dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "() });");
			} else {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "(), ");
			}
		}
		StringUtils.appendLine(output, "		if(resultList.isEmpty()) return null;");
		StringUtils.appendLine(output, "        return resultList.get(0);");
		StringUtils.appendLine(output, "    }");

		StringUtils.appendLine(output, "    @Transactional");
		StringUtils.appendLine(output, "    public int insert(" + className
				+ " dto) {");
		StringUtils.appendLine(output,
				"		int insertRecord = jdbcTemplate.update(SQL_INSERT,");
		for (int i = 0; i < cols.size(); i++) {
			DBColInfo col = cols.get(i);
			if (i == cols.size() - 1) {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, col.name)
								+ "()});");
			} else if (i == 0) {
				StringUtils.appendLine(
						output,
						"				new Object[] { dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, col.name)
								+ "(),");
			} else {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, col.name)
								+ "(), ");
			}
		}
		StringUtils.appendLine(output, "        return insertRecord;");
		StringUtils.appendLine(output, "    }");

		StringUtils.appendLine(output, "    @Transactional");
		StringUtils.appendLine(output, "    public int update(" + className
				+ " dto) {");
		StringUtils.appendLine(output,
				"		int updateRecord = jdbcTemplate.update(SQL_UPDATE,");
		for (int i = 0; i < cols.size(); i++) {
			DBColInfo col = cols.get(i);
			if (i == 0) {
				StringUtils.appendLine(
						output,
						"				new Object[] { dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, col.name)
								+ "(),");
			} else if (i == (cols.size() - 1) && keys.size() == 0) {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, col.name)
								+ "()}); ");
			} else {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, col.name)
								+ "(), ");
			}
		}
		for (int i = 0; i < keys.size(); i++) {
			if (i == keys.size() - 1) {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "()});");
			} else if (i == 0) {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "(), ");
			} else {
				StringUtils.appendLine(
						output,
						"						dto.get"
								+ CaseFormat.UPPER_UNDERSCORE.to(
										CaseFormat.UPPER_CAMEL, keys.get(i))
								+ "(), ");
			}
		}
		StringUtils.appendLine(output, "        return updateRecord;");
		StringUtils.appendLine(output, "    }");

		StringUtils.appendLine(output, "    @Transactional");
		StringUtils.appendLine(output, "    public int delete(" + className
				+ " dto) {");
		StringBuilder toDeleteKey = new StringBuilder();
		for (int i = 0; i < keys.size(); i++) {
			if (keys.size() > 1 && i == keys.size() - 1) {
				toDeleteKey.append(",dto.get"
						+ CaseFormat.UPPER_UNDERSCORE.to(
								CaseFormat.UPPER_CAMEL, keys.get(i)) + "()");
			} else if (i == 0) {
				toDeleteKey.append("dto.get"
						+ CaseFormat.UPPER_UNDERSCORE.to(
								CaseFormat.UPPER_CAMEL, keys.get(i)) + "()");
			} else {
				toDeleteKey.append(",dto.get"
						+ CaseFormat.UPPER_UNDERSCORE.to(
								CaseFormat.UPPER_CAMEL, keys.get(i)) + "()");
			}
		}
		StringUtils.appendLine(output,
				"        int deleteRecord = jdbcTemplate.update(SQL_DELETE, new Object[] {"
						+ toDeleteKey.toString() + " });");
		StringUtils.appendLine(output, "        return deleteRecord;");
		StringUtils.appendLine(output, "    }");

		StringUtils.appendLine(output, "    public int getNextId() {");
		StringUtils.appendLine(output, "        String sql = \" SELECT "
				+ dBTabInfo.tableName + "_SEQ.NEXTVAL FROM DUAL \";");
		StringUtils
				.appendLine(output,
						"        return jdbcTemplate.queryForObject(sql, Integer.class);");
		StringUtils.appendLine(output, "    }");
		StringUtils.appendLine(output, "    /*");
		StringUtils.appendLine(output, "     * End DevAss DAO Generated");
		StringUtils.appendLine(output, "     */");
		/**
		 * End SIUD
		 */

		return output.toString();
	}

	public String toResultSetGet(String dbType) {
		if (!typeDefinite.containsKey(dbType)) {
			return dbType;
		}
		return typeDefinite.get(dbType).getResultSetGet();
	}

	public List<String> getKeys(List<DBColInfo> columns) {
		List<String> keys = new ArrayList<>();
		for (DBColInfo colInfo : columns) {
			if (colInfo.constraint == null) {
				continue;
			}
			if (!colInfo.constraint.contains("P")) {
				continue;
			}
			keys.add(colInfo.name);
		}
		return keys;
	}
}
