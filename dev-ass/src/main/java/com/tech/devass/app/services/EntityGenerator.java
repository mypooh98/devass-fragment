package com.tech.devass.app.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.base.CaseFormat;
import com.tech.devass.app.models.ConfigSkipColumn;
import com.tech.devass.app.models.ConfigSkipType;
import com.tech.devass.app.models.DBColInfo;
import com.tech.devass.app.models.DBTabInfo;
import com.tech.devass.app.models.JavaTypeDef;
import com.tech.devass.app.utils.StringUtils;

public class EntityGenerator {
	
	private List<ConfigSkipType> skipTypes = new ArrayList<>();
	private List<ConfigSkipColumn> skipColumns = new ArrayList<>();
	protected Map<String, JavaTypeDef> typeDefinite;
	
	public void setTypeDefinite(Map<String, JavaTypeDef> typeDefinite){
		this.typeDefinite = typeDefinite;
	}

	public String gen(DBTabInfo dBTabInfo) {
		String tableName = CaseFormat.UPPER_UNDERSCORE.to(
				CaseFormat.UPPER_CAMEL, dBTabInfo.tableName);
		StringBuilder outputStr = new StringBuilder();
		StringUtils.appendLine(outputStr, tableName + "{");
		for (DBColInfo colInfo : dBTabInfo.columns) {
			
			if(checkSkipCondition(colInfo)){
				continue;
			}
			
			String javaType = toJavaType(colInfo.type);

			String entityProp = CaseFormat.UPPER_UNDERSCORE.to(
					CaseFormat.UPPER_CAMEL, colInfo.name);
			entityProp = Character.toLowerCase(entityProp.charAt(0))
					+ (entityProp.length() > 1 ? entityProp.substring(1) : "");

			StringUtils.appendLine(outputStr, "private " + javaType + " " + entityProp + ";");
		}
		StringUtils.appendLine(outputStr, "}");
		return outputStr.toString();
	}
	
	public boolean checkSkipCondition(DBColInfo colInfo){
		for(ConfigSkipColumn skipCol:skipColumns){
			if(skipCol.getOper().equals("startwith")){
				if(colInfo.name.startsWith(
						skipCol.getPartOfColumnName())){
					return true;
				}
			}else if(skipCol.getOper().equals("contains")){
				if(colInfo.name.contains(
						skipCol.getPartOfColumnName())){
					return true;
				}
			}else if(skipCol.getOper().equals("endwith")){
				if(colInfo.name.endsWith(
						skipCol.getPartOfColumnName())){
					return true;
				}
			}
		}
		for(ConfigSkipType skipType:skipTypes){
			if(skipType.getOper().equals("startwith")){
				if(colInfo.type.startsWith(
						skipType.getPartOfTypeName())){
					return true;
				}
			}else if(skipType.getOper().equals("contains")){
				if(colInfo.type.contains(
						skipType.getPartOfTypeName())){
					return true;
				}
			}else if(skipType.getOper().equals("endwith")){
				if(colInfo.type.endsWith(
						skipType.getPartOfTypeName())){
					return true;
				}
			}
		}
		return false;
	}

	public String toJavaType(String dbType) {
		if(!typeDefinite.containsKey(dbType)){
			return dbType;
		}
		return typeDefinite.get(dbType).getJavaType();
	}

	public void addSkipType(ConfigSkipType type) {
		skipTypes.add(type);
	}
	
	public void addSkipColumns(ConfigSkipColumn column){
		skipColumns.add(column);
	}
}
