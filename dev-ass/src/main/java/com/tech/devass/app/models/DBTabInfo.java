package com.tech.devass.app.models;

import java.util.ArrayList;
import java.util.List;

public class DBTabInfo {
	public String tableName;
	public String status;
	public List<DBColInfo> columns;
	
	public DBTabInfo(String tableName) {
		this.tableName = tableName;
		columns = new ArrayList<>();
	}
	
	public void addCol(String name, String type, String constraint){
		DBColInfo dbColInfo = new DBColInfo();
		dbColInfo.name = name;
		dbColInfo.type = type;
		dbColInfo.constraint = constraint;
		columns.add(dbColInfo);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof DBTabInfo)){
			return false;
		}
		DBTabInfo other = (DBTabInfo)obj;
		return tableName.equals(other.tableName);
	}
}
