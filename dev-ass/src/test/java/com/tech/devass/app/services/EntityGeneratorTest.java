package com.tech.devass.app.services;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.tech.devass.app.models.ConfigSkipColumn;
import com.tech.devass.app.models.ConfigSkipType;
import com.tech.devass.app.models.DBTabInfo;
import com.tech.devass.app.models.JavaTypeDef;
import com.tech.devass.app.utils.StringUtils;

public class EntityGeneratorTest {

	@Test
	public void entity_code_should_like_expected_code() {
		DBTabInfo dBTabInfo = new DBTabInfo(
				"BATCH_CONFIG");
		dBTabInfo.addCol("PARAM", "CLOB", null);
		dBTabInfo.addCol("NEXT_EXECUTE_DT",
				"TIMESTAMP(6) WITH TIME ZONE", null);
		dBTabInfo.addCol("LAST_FAIL_STACK", "NUMBER", null);
		dBTabInfo.addCol("SYSTEM_XXX", "XXX", null);

		EntityGenerator entityGenerator = new EntityGenerator();

		Map<String, JavaTypeDef> typeDefinite = new HashMap<>();
		typeDefinite.put("NUMBER", new JavaTypeDef("int", "getInt"));
		typeDefinite.put("NVARCHAR2", new JavaTypeDef("String", "getString"));
		typeDefinite.put("VARCHAR2", new JavaTypeDef("String", "getString"));
		typeDefinite.put("TIMESTAMP(6) WITH TIME ZONE", 
				new JavaTypeDef("Date", "getTimestamp"));
		typeDefinite.put("DATE", new JavaTypeDef("Date", "getTimestamp"));
		entityGenerator.setTypeDefinite(typeDefinite);

		entityGenerator.addSkipType(new ConfigSkipType("contains", "CLOB"));
		
		entityGenerator.addSkipColumns(new ConfigSkipColumn("startwith", "SYSTEM_"));

		String entityCode = entityGenerator.gen(dBTabInfo);

		StringBuilder expectedOutput = new StringBuilder();
		StringUtils.appendLine(expectedOutput, "BatchConfig{");
		StringUtils.appendLine(expectedOutput, "private Date nextExecuteDt;");
		StringUtils.appendLine(expectedOutput, "private int lastFailStack;");
		StringUtils.appendLine(expectedOutput, "}");

		System.out.println(entityCode);
		//assertEquals(expectedOutput.toString(), entityCode);
	}

}
