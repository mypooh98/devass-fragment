package com.tech.devass.app.services;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.tech.devass.app.models.ConfigSkipType;
import com.tech.devass.app.models.DBTabInfo;
import com.tech.devass.app.models.JavaTypeDef;

public class DAOGeneratorTest {

	@Test(expected = IllegalArgumentException.class)
	public void expect_IllegalArgumentException() {
		DBTabInfo dBTabInfo = new DBTabInfo(
				"BATCH_CONFIG");
		dBTabInfo.addCol("PARAM", "CLOB", null);
		dBTabInfo.addCol("NEXT_EXECUTE_DT",
				"TIMESTAMP(6) WITH TIME ZONE", null);
		dBTabInfo.addCol("LAST_FAIL_STACK", "NUMBER", null);

		DAOGenerator daoGenerator = new DAOGenerator();

		Map<String, JavaTypeDef> typeDefinite = new HashMap<>();
		typeDefinite.put("NUMBER", new JavaTypeDef("int", "getInt"));
		typeDefinite.put("NVARCHAR2", new JavaTypeDef("String", "getString"));
		typeDefinite.put("VARCHAR2", new JavaTypeDef("String", "getString"));
		typeDefinite.put("TIMESTAMP(6) WITH TIME ZONE", 
				new JavaTypeDef("Date", "getTimestamp"));
		typeDefinite.put("DATE", new JavaTypeDef("Date", "getTimestamp"));
		daoGenerator.setTypeDefinite(typeDefinite);

		daoGenerator.addSkipType(new ConfigSkipType("contains", "CLOB"));

		daoGenerator.gen(dBTabInfo);
	}

	@Test
	public void entity_code_should_like_expected_code() {
		DBTabInfo dBTabInfo = new DBTabInfo(
				"BATCH_CONFIG");
		dBTabInfo.addCol("PARAM", "CLOB", "");
		dBTabInfo.addCol("NEXT_EXECUTE_DT",
				"TIMESTAMP(6) WITH TIME ZONE", null);
		dBTabInfo.addCol("LAST_FAIL_STACK", "NUMBER", "P");

		DAOGenerator daoGenerator = new DAOGenerator();

		Map<String, JavaTypeDef> typeDefinite = new HashMap<>();
		typeDefinite.put("NUMBER", new JavaTypeDef("int", "getInt"));
		typeDefinite.put("NVARCHAR2", new JavaTypeDef("String", "getString"));
		typeDefinite.put("VARCHAR2", new JavaTypeDef("String", "getString"));
		typeDefinite.put("TIMESTAMP(6) WITH TIME ZONE", 
				new JavaTypeDef("Date", "getTimestamp"));
		typeDefinite.put("DATE", new JavaTypeDef("Date", "getTimestamp"));
		daoGenerator.setTypeDefinite(typeDefinite);

		daoGenerator.addSkipType(new ConfigSkipType("contains", "CLOB"));

		StringBuilder sb = new StringBuilder();
		sb.append("    @Autowired \n");
		sb.append("    private JdbcTemplate jdbcTemplate; \n");
		sb.append("    protected final static String SQL_SELECT = buildSelectStatement(); \n");
		sb.append("    protected final static String SQL_SELECT_ONE = buildSelectOneStatement(); \n");
		sb.append("    protected final static String SQL_INSERT = buildInsertStatement(); \n");
		sb.append("    protected final static String SQL_UPDATE = buildUpdateStatement(); \n");
		sb.append("    protected final static String SQL_DELETE = buildDeleteStatement(); \n");
		sb.append("    private final static RowMapper<BatchConfig> rowMapperBatchConfig = new RowMapper<BatchConfig>() { \n");
		sb.append("		@Override \n");
		sb.append("		public BatchConfig mapRow(ResultSet rs, int rowNum) throws SQLException { \n");
		sb.append("			BatchConfig dto = new BatchConfig(); \n");
		sb.append("			dto.setNextExecuteDt(rs.getTimestamp(\"NEXT_EXECUTE_DT\"));\n");
		sb.append("			dto.setLastFailStack(rs.getInt(\"LAST_FAIL_STACK\"));\n");
		sb.append("			return dto; \n");
		sb.append("		} \n");
		sb.append("    }; \n");
		sb.append("    private static String buildSelectStatement() { \n");
		sb.append("        StringBuilder sql = new StringBuilder(); \n");
		sb.append("        sql.append(\" SELECT \"); \n");
		sb.append("        sql.append(\"   NEXT_EXECUTE_DT \"); \n");
		sb.append("        sql.append(\"   ,LAST_FAIL_STACK \"); \n");
		sb.append("        sql.append(\" FROM BATCH_CONFIG \"); \n");
		sb.append("        return sql.toString(); \n");
		sb.append("    } \n");
		sb.append("    private static String buildSelectOneStatement() { \n");
		sb.append("        StringBuilder sql = new StringBuilder(); \n");
		sb.append("        sql.append(\" SELECT \"); \n");
		sb.append("        sql.append(\"   NEXT_EXECUTE_DT \"); \n");
		sb.append("        sql.append(\"   ,LAST_FAIL_STACK \"); \n");
		sb.append("        sql.append(\" FROM BATCH_CONFIG\"); \n");
		sb.append("        sql.append(\" WHERE \"); \n");
		sb.append("        sql.append(\"   LAST_FAIL_STACK = ? \"); \n");
		sb.append("        return sql.toString(); \n");
		sb.append("    } \n");
		sb.append("    private static String buildInsertStatement() { \n");
		sb.append("        StringBuilder sql = new StringBuilder(); \n");
		sb.append("        sql.append(\" INSERT INTO BATCH_CONFIG ( \"); \n");
		sb.append("        sql.append(\"   NEXT_EXECUTE_DT \"); \n");
		sb.append("        sql.append(\"   ,LAST_FAIL_STACK) \"); \n");
		sb.append("        sql.append(\" VALUES (?,?) \"); \n");
		sb.append("        return sql.toString(); \n");
		sb.append("    } \n");
		sb.append("    private static String buildUpdateStatement() { \n");
		sb.append("        StringBuilder sql = new StringBuilder(); \n");
		sb.append("        sql.append(\" UPDATE BATCH_CONFIG \"); \n");
		sb.append("        sql.append(\" SET \"); \n");
		sb.append("        sql.append(\"   NEXT_EXECUTE_DT = ? \"); \n");
		sb.append("        sql.append(\"   ,LAST_FAIL_STACK = ? \"); \n");
		sb.append("        sql.append(\" WHERE \"); \n");
		sb.append("        sql.append(\"   LAST_FAIL_STACK = ? \"); \n");
		sb.append("        return sql.toString(); \n");
		sb.append("    } \n");
		sb.append("    private static String buildDeleteStatement() { \n");
		sb.append("        StringBuilder sql = new StringBuilder(); \n");
		sb.append("        sql.append(\" DELETE FROM BATCH_CONFIG \"); \n");
		sb.append("        sql.append(\" WHERE \"); \n");
		sb.append("        sql.append(\"   LAST_FAIL_STACK = ? \"); \n");
		sb.append("        return sql.toString(); \n");
		sb.append("    } \n");
		sb.append("    public List<BatchConfig> selectAll() {\n");
		sb.append("        List<BatchConfig> resultList = jdbcTemplate.query(SQL_SELECT, rowMapperBatchConfig);\n");
		sb.append("        return resultList;\n");
		sb.append("    }\n");
		sb.append("    public BatchConfig selectOne(BatchConfig dto) {\n");
		sb.append("        List<BatchConfig> resultList = jdbcTemplate.query(SQL_SELECT_ONE, rowMapperBatchConfig, \n");
		sb.append("						new Object[] { dto.getLastFailStack() });\n");
		sb.append("        return resultList.get(0);\n");
		sb.append("    }\n");
		sb.append("    @Transactional\n");
		sb.append("    public int insert(BatchConfig dto) {\n");
		sb.append("		int insertRecord = jdbcTemplate.update(SQL_INSERT,\n");
		sb.append("				new Object[] { dto.getNextExecuteDt(),\n");
		sb.append("						dto.getLastFailStack()});\n");
		sb.append("        return insertRecord;\n");
		sb.append("    }\n");
		sb.append("    @Transactional\n");
		sb.append("    public int update(BatchConfig dto) {\n");
		sb.append("		int updateRecord = jdbcTemplate.update(SQL_UPDATE,\n");
		sb.append("				new Object[] { dto.getNextExecuteDt(),\n");
		sb.append("						dto.getLastFailStack(), \n");
		sb.append("						dto.getLastFailStack()});\n");
		sb.append("        return updateRecord;\n");
		sb.append("    }\n");
		sb.append("    @Transactional\n");
		sb.append("    public int delete(BatchConfig dto) {\n");
		sb.append("        int deleteRecord = jdbcTemplate.update(SQL_DELETE, new Object[] {dto.getLastFailStack() });\n");
		sb.append("        return deleteRecord;\n");
		sb.append("    }\n");
		sb.append("    public int getNextId() {\n");
		sb.append("        String sql = \" SELECT BATCH_CONFIG_SEQ.NEXTVAL FROM DUAL \";\n");
		sb.append("        return jdbcTemplate.queryForObject(sql, Integer.class);\n");
		sb.append("    }\n");
		sb.append("    /*\n");
		sb.append("     * End DevAss DAO Generated\n");
		sb.append("     */\n");
		System.out.println(daoGenerator.gen(dBTabInfo));

		//assertEquals(sb.toString(), daoGenerator.gen(dBTabInfo));
	}

}
